require 'pathname'
require 'yaml'
require 'fileutils'

module Zulip
  module Reminder
    class Config

      attr :stream, true
      attr :topic, true
      attr :debug, true
      attr_reader :key
      attr_reader :site
      attr_reader :bot_name
      attr_reader :bot_email
      attr_reader :user_name
      attr_reader :user_email

      def initialize
        @debug = false
        parse_zuliprc
        parse_config
      end

      def task_path
        if ENV['ZULIP_REMINDER_HOME']
          Pathname("#{ENV['ZULIP_REMINDER_HOME']}/task.yaml").expand_path
        else
          Pathname("~/.config/zulip-reminder/task.yaml").expand_path
        end
      end

      def debug?
        @debug
      end

      private
      def config_dir
        if ENV['ZULIP_REMINDER_HOME']
          ENV['ZULIP_REMINDER_HOME']
        else
          Pathname("~/.config/zulip-reminder").expand_path
        end
      end

      def zulip_rc_path
        Pathname("#{ENV['HOME']}/.zuliprc").expand_path
      end

      def parse_zuliprc
        open(zulip_rc_path) do |file|
          file.each_line do |line|
            case line
            when /email=(.*)/
              @user_email = $1
            when /key=(.*)/
              @key = $1
            when /site=(.*)/
              @site = $1
            end
          end
        end
      end

      def parse_config
        FileUtils.mkdir_p(config_dir)
        path = File.join(config_dir, "zulip-reminder.conf")
        yaml = YAML.load_file(path)
        @stream = yaml["stream"]
        if yaml["bot"]
          if yaml["bot"]["name"]
            @bot_name = yaml["bot"]["name"]
          end
          if yaml["bot"]["email"]
            @bot_email = yaml["bot"]["email"]
          end
        end
        @topic = yaml["topic"]
        @user_name = yaml["user"]
        if yaml["debug"]
          if yaml["debug"]["enabled"]
            @debug = true
          end
        end
      end
    end
  end
end
