module Zulip
  module Reminder
    class Task
      def initialize(options={})
        @output = options[:output] || $stdout
        @config = options[:config] || Zulip::Reminder::Config.new
        @client = Zulip::Client.new(site: @config.site,
                                    username: @config.user_email,
                                    api_key: @config.key)
      end

      def add(content: "")
        if content.empty?
          @output.puts("")
          raise EmptyTaskError
        end
        @output.puts "Register: #{content}"
        if @config.debug?
          @client.send_private_message(to: @config.user_email, content: content)
        else
          @client.send_public_message(to: @config.stream, subject: @config.topic, content: content)
        end
        result = fetch_bot_response
        parse_created_jobs(result)
        if @job_id.nil?
          puts "Failed to confirm job"
          return
        end

        if @cron_content.empty?
          puts "Failed to confirm task"
          return
        end
        parse_task_content
        save
      end

      def job_id
        @job_id
      end

      def description
        @description
      end

      private
      def fetch_bot_response
        params = {}
        params["use_first_unread_anchor"] = true
        params["num_before"] = 2
        params["num_after"] = 10
        narrow = []
        unless @config.debug?
          narrow = [
            {"operand": @config.stream, "operator":"stream"},
            {"operand": @config.topic, "operator":"topic"}
          ]
          params["narrow"] = JSON.generate(narrow)
        end
        data = URI.encode_www_form(params)
        uri = URI("#{@config.site}/api/v1/messages?" + data)
        result = nil
        Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
          request = Net::HTTP::Get.new(uri)
          request.basic_auth(@config.user_email, @config.key)
          response = http.request(request)
          result = JSON.parse(response.body)
        end
        result
      end

      def strip_html_tags(content)
        content.gsub(/(<[^>]*>)/) {""}
      end

      def related_message?(sender_email)
        [@config.user_email, @config.bot_email].include?(sender_email)
      end

      def parse_created_jobs(response)
        @job_id = nil
        @cron_content = ""
        response["messages"].each do |message|
          unless related_message?(message["sender_email"])
            next
          end
          content = strip_html_tags(message["content"])
          @output.puts "Check: #{content}" if @config.debug?
          case  message["sender_email"]
          when @config.user_email
            @cron_content = content
            @output.puts "Fetched task: #{content}" if @config.debug?
          else
            content.match(/^Job (.+) created/) do |matched|
              @job_id = matched[1]
              @output.puts "Fetched job id: #{job_id}" if @config.debug?
            end
          end
        end
      end

      def display_name(name)
        name.gsub('*', '')
      end

      def parse_task_content
        @up_to = nil
        @description = ""
        @cron_content.match(/add job "(.+)" #{display_name(@config.bot_name)} echo #{display_name(@config.user_name)} (.+)$/) do |matched|
          @up_to = matched[1]
          @description = matched[2]
        end
      end

      def save
        data = {}
        params = {}
        path = @config.task_path
        if path.exist?
          data = YAML.load_file(path.to_s)
        end
        params = {
          stream: @config.stream,
          topic: @config.topic,
          cron_content: @cron_content,
          description: @description
        }
        time = parse_oneshot_time(@up_to)
        if time
          params[:up_to] = time
        end
        data[@job_id] = params
        open(path.to_s, "w+") do |file|
          file.puts(YAML.dump(data))
        end
      end

      def parse_oneshot_time(content)
        oneshot_time = nil
        content.match(/(\d{,2}) (\d{,2}) (\d{,2}) (\d{,2}) (.+)/) do |matched|
          oneshot_time = Time.parse("#{$4.to_i}/#{$3.to_i} #{$2.to_i}:#{$1.to_i}")
        end
        oneshot_time
      end
    end
  end
end
