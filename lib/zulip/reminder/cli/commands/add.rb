# frozen_string_literal: true

require 'net/http'
require 'time'
require 'zulip/client'
require_relative '../command'
require 'zulip/reminder/config'
require 'zulip/reminder/task'

module Zulip
  module Reminder
    module Cli
      module Commands
        class Add < Zulip::Reminder::Cli::Command
          def initialize(timestamp, message, options)
            @timestamp = timestamp
            @message = message
            @options = options
            @config = Zulip::Reminder::Config.new
            if options[:stream]
              @config.stream = options[:stream]
            end
            if options[:topic]
              @config.topic = options[:topic]
            end
          end

          def execute(input: $stdin, output: $stdout)
            task = Zulip::Reminder::Task.new(config: @config)
            content = "#{@config.bot_name} add job \"#{convert_timestamp(@timestamp)}\" #{@config.bot_name} echo #{@config.user_name} #{@message}"
            task.add(content: content)
          end

          private
          def convert_timestamp(content)
            t = Time.parse(`LANG=C date -d "#{content}"`)
            "#{t.min} #{t.hour} #{t.day} #{t.month} *"
          end
        end
      end
    end
  end
end
